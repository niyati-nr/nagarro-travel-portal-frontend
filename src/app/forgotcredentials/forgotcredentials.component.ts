import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Employee } from '../models/employeemodel';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-forgotcredentials',
  templateUrl: './forgotcredentials.component.html',
  styleUrls: ['./forgotcredentials.component.css']
})
export class ForgotcredentialsComponent implements OnInit {
  employee:Employee={ };
  userId:Number;
  servererror = false;
  @ViewChild('gettingcredentials', { static: true }) GettingCredentialsSpinner: ElementRef;
  constructor(private employeeService:EmployeeService,private router: Router) { }

  ngOnInit(): void {
    this.GettingCredentialsSpinner.nativeElement.style.display="none";
  }
  async onSubmit(forgotCredentialsForm:NgForm){
    this.GettingCredentialsSpinner.nativeElement.style.display="block";
    var email:string = JSON.stringify(forgotCredentialsForm.value.email);
    await this.employeeService.checkEmail(email)
    .subscribe(answer=>{
      this.userId=answer;
      if(this.userId===-1){
        this.servererror = true;
      }
      else{
        this.servererror = false;
        this.getCredentials(this.userId);
      }
    });
    this.GettingCredentialsSpinner.nativeElement.style.display="none";
    }
    async getCredentials(id:Number){
      await this.employeeService.forgotCredentials(id).subscribe(response => {
        if(response.status==200){
          alert("Credentials have been mailed to your e-mail address.")
          this.GettingCredentialsSpinner.nativeElement.style.display="none";
          this.router.navigate(['/login']);
        }
      });
    }
}
