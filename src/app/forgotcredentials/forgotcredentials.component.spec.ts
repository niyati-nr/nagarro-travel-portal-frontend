import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotcredentialsComponent } from './forgotcredentials.component';

describe('ForgotcredentialsComponent', () => {
  let component: ForgotcredentialsComponent;
  let fixture: ComponentFixture<ForgotcredentialsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForgotcredentialsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotcredentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
