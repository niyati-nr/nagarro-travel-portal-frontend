import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CountryService } from '../services/country.service';
import { RegionService } from '../services/region.service';
import { CityService } from '../services/city.service';
import { Country } from '../models/countrymodel';
import { Region } from '../models/regionmodel';
import { City } from '../models/citymodel';
import {RequestStatus, RequestPriority, RequestType, TicketRequest, ExpenseHandler} from '../models/ticketrequest';
import { EmployeeService } from '../services/employee.service';
import { LocalstorageService } from '../services/localstorage.service';
import { DatePipe } from '@angular/common';
import {TicketRequestForm} from '../models/ticketrequestformmodel';
import { TicketrequestService } from '../services/ticketrequest.service';
import { Employee } from '../models/employeemodel';
@Component({
  selector: 'app-editticket',
  templateUrl: './editticket.component.html',
  styleUrls: ['./editticket.component.css']
})
export class EditticketComponent implements OnInit {
  @Input() ticketRequest:TicketRequest;
  ticketRequestForm:TicketRequestForm={}
  requestStatus = RequestStatus;
  requestPriority = RequestPriority;
  requestType = RequestType;
  expenseHandler = ExpenseHandler;
  countries:Country[]=[]
  regions1:Region[]=[]
  cities1:City[]=[]
  regions2:Region[]=[]
  cities2:City[]=[]
  showRegions1=false;
  showCities1=false;
  showRegions2=false;
  showCities2=false;
  showDBRegion1=true;
  showDBCity1=true;
  showDBRegion2=true;
  showDBCity2=true;
  employee:Employee;
  id:string;
  keys = Object.keys;
  constructor(private countryService:CountryService, private regionService:RegionService, private cityService:CityService, private route: ActivatedRoute,private router: Router, private datePipe:DatePipe, private ticketRequestService:TicketrequestService, private localStorageService:LocalstorageService) { }
  ngOnInit(): void {
    if(this.localStorageService.getData("employee")==null&&this.localStorageService.getData("employee")==undefined){
      this.router.navigate(['/login']);
    }
    if(this.localStorageService.getData("isAdmin")=="true"){
      this.router.navigate(['/alltickets']);
    }
    this.getCountries().then(()=>{
      this.getTicket();
    });
    this.id = this.route.snapshot.paramMap.get('id');
    this.employee = JSON.parse(this.localStorageService.getData("employee"));
  }
  async getTicket(){
    await this.ticketRequestService.getTicketById(parseInt(this.id))
    .subscribe(ticketRequest=>{
      this.ticketRequest = ticketRequest;
    });
  }
  onSubmit(ngForm:NgForm){
    this.ticketRequestForm.request_priority = ngForm.value.request_priority;
    this.ticketRequestForm.request_type = ngForm.value.request_type;
    this.ticketRequestForm.additional_details = ngForm.value.additional_details;
    this.ticketRequestForm.amount_limit = ngForm.value.amount_limit;
    this.ticketRequestForm.destination_city = ngForm.value.city2.id;
    this.ticketRequestForm.origin_city = ngForm.value.city1.id;
    this.ticketRequestForm.end_date = this.datePipe.transform(new Date(ngForm.value.end_date),"dd/MM/yyyy");
    this.ticketRequestForm.start_date = this.datePipe.transform(new Date(ngForm.value.start_date),"dd/MM/yyyy");
    this.ticketRequestForm.expense_handler = ngForm.value.expense_handler;
    this.ticketRequestForm.passport_number = ngForm.value.passport_number;
    this.ticketRequestForm.project_name = ngForm.value.project_name;
    this.ticketRequestForm.travel_approver = ngForm.value.travel_approver;
    this.ticketRequestForm.travel_duration = ngForm.value.travel_duration;
    console.log(this.ticketRequestForm);
    this.editTicket();
  }
  async onChangeRegion(newRegionId,elementName:string){
    // if(elementName=="region1"){
    //   this.PrimaryAddressSpinner.nativeElement.style.display="block";
    // }
    // else{
    //   this.SecondaryAddressSpinner.nativeElement.style.display="block";
    // }
    console.log(newRegionId);
    await this.cityService.getCities(newRegionId)
    .subscribe(cities=>{
      if(elementName=="region1"){
        this.cities1=cities;
        this.showCities1=true;
        // this.PrimaryAddressSpinner.nativeElement.style.display="none";
      }
      else if(elementName=="region2"){
        this.cities2=cities;
        this.showCities2=true;
        // this.SecondaryAddressSpinner.nativeElement.style.display="none";
      }
    });
  }
  async onChangeCountry(newCountryId,elementName:string){
    
    if(elementName=="country1"){
      // this.PrimaryAddressSpinner.nativeElement.style.display="block";
      this.showDBRegion1=false;
      this.showDBCity1=false;
    }
    else{
      // this.SecondaryAddressSpinner.nativeElement.style.display="block";
      this.showDBRegion2=false;
      this.showDBCity2=false;
    }
    await this.regionService.getRegions(newCountryId)
    .subscribe(regions=>{
      if(elementName=="country1"){
        this.regions1=regions;
        this.showRegions1=true;
        // this.PrimaryAddressSpinner.nativeElement.style.display="none";
      }
      else if(elementName=="country2"){
        this.regions2=regions;
        this.showRegions2=true;
        // this.SecondaryAddressSpinner.nativeElement.style.display="none";
      }
    });
  }
  async getCountries(){
    await this.countryService.getCountries()
    .subscribe(countries=>this.countries=countries);
  }
  async editTicket(){
    await this.ticketRequestService.editTicket(this.ticketRequestForm,this.ticketRequest.request_id).subscribe(ticketRequest => {
      this.ticketRequest = ticketRequest;
      if(this.ticketRequest==null||this.ticketRequest==undefined){
        alert("Error Editing Ticket. Please try again.");
        window.location.reload();
      }
      else{
        alert("Ticket updated successfully!");
        this.router.navigate(['/listtickets']);
      }
    });
  }

}
