import { Component, OnInit } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import {NgForm} from '@angular/forms';
import {EmployeeService} from '../services/employee.service';
import {Employee} from '../models/employeemodel';
import {LocalstorageService} from '../services/localstorage.service';
import { CountryService } from '../services/country.service';
import {Country} from '../models/countrymodel';
import {Region} from '../models/regionmodel';
import {City} from '../models/citymodel';
import {RegisterForm} from '../models/registerformmodel';
import { RegionService } from '../services/region.service';
import { CityService } from '../services/city.service';
import { element } from 'protractor';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  countries:Country[]=[]
  regions1:Region[]=[]
  cities1:City[]=[]
  regions2:Region[]=[]
  cities2:City[]=[]
  selectedCity1:City={}
  selectedCity2:City={}
  showRegions1=false;
  showCities1=false;
  showRegions2=false;
  showCities2=false;
  secondaryAddressChecked=false;
  registerForm:RegisterForm={};
  employee:Employee={};
  servererror=false;
  emailexistserror = false;
  userId:Number;
  @ViewChild('loadinguser', { static: true }) LoadingUserSpinner: ElementRef;
  @ViewChild('secondaryaddressspinner', { static: true }) SecondaryAddressSpinner: ElementRef;
  @ViewChild('primaryaddressspinner', { static: true }) PrimaryAddressSpinner: ElementRef;
  constructor(private employeeService:EmployeeService,private localstorageService:LocalstorageService,private countryService:CountryService, private regionService:RegionService, private cityService:CityService, private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    if(this.localstorageService.getData("employee")!=null&&this.localstorageService.getData("employee")!=undefined){
      this.router.navigate(['/dashboard'])
    }
    this.getCountries();
    this.LoadingUserSpinner.nativeElement.style.display="none";
    this.PrimaryAddressSpinner.nativeElement.style.display="none";
    this.SecondaryAddressSpinner.nativeElement.style.display="none";
  }
  async onSubmit(registerForm:NgForm){
    this.LoadingUserSpinner.nativeElement.style.display="block";
    await this.employeeService.checkEmail(registerForm.value.email)
    .subscribe(answer=>{
      this.userId=answer;
      if(this.userId!==-1){
        this.emailexistserror = true;
        this.LoadingUserSpinner.nativeElement.style.display="none";
      }
      else{
        this.servererror = false;
        this.registerForm.businessunit = registerForm.value.businessunit;
        this.registerForm.email = registerForm.value.email;
        this.registerForm.firstname = registerForm.value.firstname;
        this.registerForm.lastname = registerForm.value.lastname;
        this.registerForm.jobtitle = registerForm.value.jobtitle;
        this.registerForm.telephone = registerForm.value.telephone
        //Setting Primary Address
        this.registerForm.addressline1 = registerForm.value.addressline1;
        this.registerForm.zipcode1 = registerForm.value.zipcode1;
        this.registerForm.addresstype1 = "Primary";
        this.registerForm.city1 = registerForm.value.city1.name;
        this.registerForm.state1 = registerForm.value.city1.ref_region.name;
        this.registerForm.country1 = registerForm.value.city1.ref_region.ref_country.name;
        //Setting Secondary Address
        if(!this.secondaryAddressChecked){
          //do nothing
        }
        else{
          this.registerForm.addressline2 = registerForm.value.addressline2;
          this.registerForm.zipcode2 = registerForm.value.zipcode2;
          this.registerForm.addresstype2 = "Secondary";
          this.registerForm.city2 = registerForm.value.city2.name;
          this.registerForm.state2 = registerForm.value.city2.ref_region.name;
          this.registerForm.country2 = registerForm.value.city2.ref_region.ref_country.name;
        }
        this.registerEmployee();
      }
    });
    
    
  }
  onChangeCity(registerForm:NgForm){
    // console.log(registerForm.value);
    //do nothing
  }
  async onChangeRegion(newRegionId,elementName:string){
    if(elementName=="region1"){
      this.PrimaryAddressSpinner.nativeElement.style.display="block";
    }
    else{
      this.SecondaryAddressSpinner.nativeElement.style.display="block";
    }
    console.log(newRegionId);
    await this.cityService.getCities(newRegionId)
    .subscribe(cities=>{
      if(elementName=="region1"){
        this.cities1=cities;
        this.showCities1=true;
        this.PrimaryAddressSpinner.nativeElement.style.display="none";
      }
      else if(elementName=="region2"){
        this.cities2=cities;
        this.showCities2=true;
        this.SecondaryAddressSpinner.nativeElement.style.display="none";
      }
    });
  }
  async onChangeCountry(newCountryId,elementName:string){
    if(elementName=="country1"){
      this.PrimaryAddressSpinner.nativeElement.style.display="block";
    }
    else{
      this.SecondaryAddressSpinner.nativeElement.style.display="block";
    }
    await this.regionService.getRegions(newCountryId)
    .subscribe(regions=>{
      if(elementName=="country1"){
        this.regions1=regions;
        this.showRegions1=true;
        this.PrimaryAddressSpinner.nativeElement.style.display="none";
      }
      else if(elementName=="country2"){
        this.regions2=regions;
        this.showRegions2=true;
        this.SecondaryAddressSpinner.nativeElement.style.display="none";
      }
    });
  }
  async getCountries(){
    await this.countryService.getCountries()
    .subscribe(countries=>this.countries=countries);
  }
  toggleSecondaryAddress(event){
    this.secondaryAddressChecked = !this.secondaryAddressChecked;
  }
  removeServerError(){
    this.emailexistserror = false;
    this.servererror = false;
  }
  async registerEmployee(){
    await this.employeeService.register(this.registerForm).subscribe(employee => {
      this.employee = employee;
      if(this.employee==null){
        this.servererror=true;
      }
      else{
        alert("Registration Successful!\n\nYour credentials have been mailed to your e-mail address.");
        this.localstorageService.setData(employee,"profile");
        this.localstorageService.setData(this.registerForm,"registerFormData");
        this.router.navigate(['/profile']);
      }
      this.LoadingUserSpinner.nativeElement.style.display="none";
    });
  }
}
