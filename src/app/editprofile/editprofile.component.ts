import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Employee } from '../models/employeemodel';
import { EmployeeService } from '../services/employee.service';
import { LocalstorageService } from '../services/localstorage.service';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { CountryService } from '../services/country.service';
import { RegionService } from '../services/region.service';
import { CityService } from '../services/city.service';
import { RegisterForm } from '../models/registerformmodel';
import { Country } from '../models/countrymodel';
import { Region } from '../models/regionmodel';
import { City } from '../models/citymodel';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {
  @Input() localEmployeeForm?: RegisterForm;
  editEmployeeForm:RegisterForm={};
  countries:Country[]=[]
  regions1:Region[]=[]
  cities1:City[]=[]
  regions2:Region[]=[]
  cities2:City[]=[]
  selectedCity1:City={}
  selectedCity2:City={}
  showRegions1=false;
  showCities1=false;
  showRegions2=false;
  showCities2=false;
  secondaryAddressChecked=false;
  employee:Employee={};
  servererror=false;
  emailexistserror = false;
  userId:Number;
  
  @ViewChild('loadinguser', { static: true }) LoadingUserSpinner: ElementRef;
  @ViewChild('secondaryaddressspinner', { static: true }) SecondaryAddressSpinner: ElementRef;
  @ViewChild('primaryaddressspinner', { static: true }) PrimaryAddressSpinner: ElementRef;
  constructor(private employeeService:EmployeeService,private localstorageService:LocalstorageService,private countryService:CountryService, private regionService:RegionService, private cityService:CityService, private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    if(this.localstorageService.getData("registerFormData")==undefined){
      this.router.navigate(['/login']);
    }
    if(this.localstorageService.getData("isAdmin")=="true"){
      this.router.navigate(['/alltickets']);
    }
    this.localEmployeeForm = JSON.parse(this.localstorageService.getData("registerFormData"));
    this.getCountries();
    this.LoadingUserSpinner.nativeElement.style.display="none";
    this.PrimaryAddressSpinner.nativeElement.style.display="none";
    this.SecondaryAddressSpinner.nativeElement.style.display="none";
  }
  async onSubmit(editEmployeeForm:NgForm){
    this.LoadingUserSpinner.nativeElement.style.display="block";
        this.servererror = false;
        this.editEmployeeForm.businessunit = editEmployeeForm.value.businessunit;
        this.editEmployeeForm.email = editEmployeeForm.value.email;
        this.editEmployeeForm.firstname = editEmployeeForm.value.firstname;
        this.editEmployeeForm.lastname = editEmployeeForm.value.lastname;
        this.editEmployeeForm.jobtitle = editEmployeeForm.value.jobtitle;
        this.editEmployeeForm.telephone = editEmployeeForm.value.telephone
        //Setting Primary Address
        this.editEmployeeForm.addressline1 = editEmployeeForm.value.addressline1;
        this.editEmployeeForm.zipcode1 = editEmployeeForm.value.zipcode1;
        this.editEmployeeForm.addresstype1 = "Primary";
        this.editEmployeeForm.city1 = editEmployeeForm.value.city1.name;
        this.editEmployeeForm.state1 = editEmployeeForm.value.city1.ref_region.name;
        this.editEmployeeForm.country1 = editEmployeeForm.value.city1.ref_region.ref_country.name;
        //Setting Secondary Address
        if(!this.secondaryAddressChecked){
          //do nothing
        }
        else{
          this.editEmployeeForm.addressline2 = editEmployeeForm.value.addressline2;
          this.editEmployeeForm.zipcode2 = editEmployeeForm.value.zipcode2;
          this.editEmployeeForm.addresstype2 = "Secondary";
          this.editEmployeeForm.city2 = editEmployeeForm.value.city2.name;
          this.editEmployeeForm.state2 = editEmployeeForm.value.city2.ref_region.name;
          this.editEmployeeForm.country2 = editEmployeeForm.value.city2.ref_region.ref_country.name;
        }
        this.updateEmployee();
  }
  onChangeCity(editEmployeeForm:NgForm){
    // console.log(editEmployeeForm.value);
    //do nothing
  }
  async onChangeRegion(newRegionId,elementName:string){
    if(elementName=="region1"){
      this.PrimaryAddressSpinner.nativeElement.style.display="block";
    }
    else{
      this.SecondaryAddressSpinner.nativeElement.style.display="block";
    }
    console.log(newRegionId);
    await this.cityService.getCities(newRegionId)
    .subscribe(cities=>{
      if(elementName=="region1"){
        this.cities1=cities;
        this.showCities1=true;
        this.PrimaryAddressSpinner.nativeElement.style.display="none";
      }
      else if(elementName=="region2"){
        this.cities2=cities;
        this.showCities2=true;
        this.SecondaryAddressSpinner.nativeElement.style.display="none";
      }
    });
  }
  async onChangeCountry(newCountryId,elementName:string){
    if(elementName=="country1"){
      this.PrimaryAddressSpinner.nativeElement.style.display="block";
    }
    else{
      this.SecondaryAddressSpinner.nativeElement.style.display="block";
    }
    await this.regionService.getRegions(newCountryId)
    .subscribe(regions=>{
      if(elementName=="country1"){
        this.regions1=regions;
        this.showRegions1=true;
        this.PrimaryAddressSpinner.nativeElement.style.display="none";
      }
      else if(elementName=="country2"){
        this.regions2=regions;
        this.showRegions2=true;
        this.SecondaryAddressSpinner.nativeElement.style.display="none";
      }
    });
  }
  async getCountries(){
    await this.countryService.getCountries()
    .subscribe(countries=>this.countries=countries);
  }
  toggleSecondaryAddress(event){
    this.secondaryAddressChecked = !this.secondaryAddressChecked;
  }
  removeServerError(){
    this.emailexistserror = false;
    this.servererror = false;
  }
  async updateEmployee(){
    if(this.localstorageService.getData("employee")==null||this.localstorageService.getData("employee")==undefined){
      var employeeTemp :Employee = JSON.parse(this.localstorageService.getData("profile"));
    }
    else{
      var employeeTemp:Employee = JSON.parse(this.localstorageService.getData("employee"));
    }
    await this.employeeService.editProfile(this.editEmployeeForm, employeeTemp.emp_id).subscribe(employee => {
      this.employee = employee;
      if(this.employee==null){
        this.servererror=true;
      }
      else{
        alert("Profile updated successfully");
        this.localstorageService.setData(employee,"profile");
        this.localstorageService.setData(employee,"employee");
        this.localstorageService.setData(this.editEmployeeForm,"registerFormData");
        this.router.navigate(['/profile']);
      }
      this.LoadingUserSpinner.nativeElement.style.display="none";
    });
  }
}
