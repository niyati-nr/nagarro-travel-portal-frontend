import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../models/employeemodel';
import { EmployeeService } from '../services/employee.service';
import { LocalstorageService } from '../services/localstorage.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  employee:Employee;
  title="Hop Sphere";
  showLoginRegister:boolean = false;
  showAdminTemplate:boolean=false;
  constructor(private localStorageService:LocalstorageService,private employeeService:EmployeeService, private router:Router) { }

  ngOnInit(): void {
    if(this.localStorageService.getData("employee")==null||this.localStorageService.getData("employee")==undefined){
      this.showLoginRegister = true;
    }
    else{
      this.showLoginRegister = false;
      this.employee = JSON.parse(this.localStorageService.getData("employee"));
      if(this.employee.username=="admin@nagarro.com"){
        this.showAdminTemplate = true;
      }
      else{
        this.showAdminTemplate = false;
      }
    }
  }
  async logOut(){
    await this.employeeService.logOut();
    this.router.navigateByUrl('/login');
  }

}
