import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// Import library module
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { TicketRequestsComponent } from './ticket-requests/ticket-requests.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { AppRoutingModule } from './app-routing.module';
import { EmployeeprofileComponent } from './employeeprofile/employeeprofile.component';
import { EmployeedashboardComponent } from './employeedashboard/employeedashboard.component';
import { ForgotcredentialsComponent } from './forgotcredentials/forgotcredentials.component';
import { NavigationComponent } from './navigation/navigation.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { RaiseticketComponent } from './raiseticket/raiseticket.component';
import {DatePipe} from '@angular/common';
import { TicketdetailComponent } from './ticketdetail/ticketdetail.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { EditticketComponent } from './editticket/editticket.component';
import { NgApexchartsModule } from "ng-apexcharts";
import { AllticketsComponent } from './alltickets/alltickets.component';
import { ActiveticketsComponent } from './activetickets/activetickets.component';
import { AdminactionComponent } from './adminaction/adminaction.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomePageComponent } from './home-page/home-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TicketRequestsComponent,
    RegisterComponent,
    EmployeeprofileComponent,
    EmployeedashboardComponent,
    ForgotcredentialsComponent,
    NavigationComponent,
    EditprofileComponent,
    RaiseticketComponent,
    TicketdetailComponent,
    EditticketComponent,
    AllticketsComponent,
    ActiveticketsComponent,
    AdminactionComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    NgApexchartsModule,
    NgbModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
