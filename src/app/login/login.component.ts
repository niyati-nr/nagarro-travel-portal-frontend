import { Component, OnInit } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import {LoginModel} from '../models/loginmodel';
import {NgForm} from '@angular/forms';
import {EmployeeService} from '../services/employee.service';
import {Employee} from '../models/employeemodel';
import {LocalstorageService} from '../services/localstorage.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginModel:LoginModel={ };
  employee:Employee={ };
  servererror=false;
  @ViewChild('loadinguser', { static: true }) LoadingUserSpinner: ElementRef;
  constructor(private employeeService:EmployeeService, private localstorageService:LocalstorageService, private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    if(this.localstorageService.getData("employee")!=null&&this.localstorageService.getData("employee")!=undefined){
      if(this.localstorageService.getData("isAdmin")=="true"){
        this.router.navigate(['/alltickets']);
      }
      else{
        this.router.navigate(['/dashboard'])
      }
    }
    this.LoadingUserSpinner.nativeElement.style.display="none";
  }
  async onSubmit(loginForm: NgForm) {
    this.LoadingUserSpinner.nativeElement.style.display="block";
    this.loginModel.username=loginForm.value.username;
    this.loginModel.password=loginForm.value.password;
    await this.employeeService.login(this.loginModel).subscribe(employee => {
      this.employee = employee;
      if(this.employee!=null&&this.employee!=undefined){
        this.localstorageService.setData(this.employee,"employee");
        if(this.loginModel.username == "admin@nagarro.com"){
          this.localstorageService.setData(true,"isAdmin");
          this.router.navigate(['/alltickets']);
        }
        else{
          this.localstorageService.setData(false,"isAdmin");
          this.router.navigate(['/dashboard']);
        }
      }
      else{
        this.servererror = true;
      }
      this.LoadingUserSpinner.nativeElement.style.display="none";
    });
  }
  removeServerError(){
    this.servererror = false;
  }
  forgotCredentials(){
    this.router.navigate(['/forgotcredentials']);
  }

}
