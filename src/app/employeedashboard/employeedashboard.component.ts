import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalstorageService } from '../services/localstorage.service';

@Component({
  selector: 'app-employeedashboard',
  templateUrl: './employeedashboard.component.html',
  styleUrls: ['./employeedashboard.component.css']
})
export class EmployeedashboardComponent implements OnInit {

  constructor(private localstorageService:LocalstorageService, private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    if(this.localstorageService.getData("employee")==null||(this.localstorageService.getData("employee")=="{}")){
      this.router.navigate(['/login'])
    }
    if(this.localstorageService.getData("isAdmin")=="true"){
      this.router.navigate(['/alltickets']);
    }
  }

}
