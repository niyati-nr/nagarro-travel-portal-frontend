import { City } from "./citymodel";
import { Employee } from "./employeemodel";

export interface TicketRequest {
    request_id?:number,
    requestType?:RequestType,
    requestPriority?:RequestPriority,
    requestStatus?:RequestStatus,
    origin_city?:City,
    destination_city?:City,
    start_date?:Date,
    end_date?:Date,
    passport_number?:string,
    project_name?:string,
    expense_handler?:ExpenseHandler,
    travel_approver?:string,
    travel_duration?:string,
    amount_limit?:string,
    additional_details?:string,
    admin_remarks?:string,
    ref_employee?:Employee,
    lastUpdated?:string,
    links?:Object[]
  }
  export enum RequestType {
    VISA="VISA",
    WORK_PERMIT="WORK PERMIT",
    TRAVEL_TICKETS="TRAVEL TICKETS",
    HOTEL_STAY="HOTEL STAY"
  }
  export enum RequestPriority {
    NORMAL="NORMAL",
    URGENT="URGENT",
    IMMEDIATE="IMMEDIATE"
  }
  export enum RequestStatus{
      SUBMITTED="SUBMITTED",
      RESUBMITTED="RESUBMITTED",
      IN_PROCESS="IN PROCESS",
      APPROVED="APPROVED",
      REJECTED="REJECTED",
      DONE="DONE"
  }
  export enum ExpenseHandler{
    NAGARRO="NAGARRO",
    CLIENT="CLIENT"
}