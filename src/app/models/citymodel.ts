import {Region} from './regionmodel';

export interface City{
    id?: number,
    name?: string,
    ref_region?:Region
}