export interface Employee{
    emp_id?: number,
    firstname?: string,
    lastname?: string,
    businessunit?: string,
    jobtitle?: string,
    email?: string,
    telephone?: string,
    primaryaddress?: Address,
    secondaryaddress?: Address,
    username?: string,
    password?: string,
    links?:Object[]
}
export interface Address{
    address_id?: number,
    addressLine?: string,
    city?: string,
    state?: string,
    zipcode?: string,
    country?: string,
    addressType?: string
}