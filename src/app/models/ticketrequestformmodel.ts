export interface TicketRequestForm{
    request_type?:string,
    request_priority?:string,
    origin_city?:number,
    destination_city?:number,
    start_date?:string,
    end_date?:string,
    passport_number?:string,
    project_name?:string,
    expense_handler?:string,
    travel_approver?:string,
    travel_duration?:string,
    amount_limit?:string,
    additional_details?:string,
    admin_remarks?:string
}