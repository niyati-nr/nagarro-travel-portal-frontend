import {Country} from './countrymodel';

export interface Region{
    id?: number,
    name?: string,
    code?: string,
    ref_country?:Country
}