import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import{RequestStatus, TicketRequest} from '../models/ticketrequest';
import { TicketRequestForm } from '../models/ticketrequestformmodel';
import { LocalstorageService } from '../services/localstorage.service';
import { TicketrequestService } from '../services/ticketrequest.service';
declare var $:any;
@Component({
  selector: 'app-activetickets',
  templateUrl: './activetickets.component.html',
  styleUrls: ['./activetickets.component.css']
})
export class ActiveticketsComponent implements OnInit {

  tickets:TicketRequest[]
  requestStatus = RequestStatus;
  ticket:TicketRequestForm;
  p: number = 1;
  keys = Object.keys;
  constructor(private localStorageService:LocalstorageService,private ticketRequestService:TicketrequestService, private router:Router) { }

  ngOnInit(): void {
    if(this.localStorageService.getData("employee")==null&&this.localStorageService.getData("employee")==undefined){
      this.router.navigate(['/login']);
    }
    if(this.localStorageService.getData("isAdmin")!="true"){
      this.router.navigate(['/dashboard']);
    }
    this.getActiveTickets();
  }
  async getActiveTickets(){
    await this.ticketRequestService.getActiveTickets()
    .subscribe(tickets=>{
      this.tickets = tickets;
      this.tickets = this.tickets.reverse();
    })
  }
  openModal(){
    $('#actionsModal').modal('show');
  }
  closeModal(){
    $('#actionsModal').modal('hide');
  }
  onSubmit(ngForm:NgForm, ticket_id){
    if(ngForm.value.request_status!=undefined){
      this.ticketRequestService.changeStatus(ticket_id,ngForm.value.request_status);
    }
    if(ngForm.value.admin_remarks!=undefined){
      this.ticket.admin_remarks = ngForm.value.admin_remarks;
      this.ticketRequestService.editTicket(this.ticket,ticket_id);
    }
    alert("Ticket updated successfully.");
    this.router.navigate(['/alltickets']);
  }
}
