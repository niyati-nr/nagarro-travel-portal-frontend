import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveticketsComponent } from './activetickets.component';

describe('ActiveticketsComponent', () => {
  let component: ActiveticketsComponent;
  let fixture: ComponentFixture<ActiveticketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActiveticketsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveticketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
