import { Component, OnInit } from '@angular/core';
import { Employee } from '../models/employeemodel';
import { EmployeeService } from '../services/employee.service';
import { LocalstorageService } from '../services/localstorage.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { RegisterForm } from '../models/registerformmodel';
@Component({
  selector: 'app-employeeprofile',
  templateUrl: './employeeprofile.component.html',
  styleUrls: ['./employeeprofile.component.css']
})
export class EmployeeprofileComponent implements OnInit {

  constructor(private employeeService:EmployeeService, private localstorageService:LocalstorageService,private location: Location, private router:Router) { }
  employee:Employee={};
  showSecondaryAddress:boolean=false;
  showLogin:boolean=false;
  ngOnInit(): void {
    if(JSON.parse(this.localstorageService.getData("employee"))==undefined&&JSON.parse(this.localstorageService.getData("profile"))==undefined){
      this.router.navigate(['/login']);
    }
    if(this.localstorageService.getData("isAdmin")=="true"){
      this.router.navigate(['/alltickets']);
    }
    this.employee = JSON.parse(this.localstorageService.getData("employee"));
    if(this.employee==null||this.employee==undefined){
      this.showLogin = true;
      this.employee = JSON.parse(this.localstorageService.getData("profile"));
    }
    else{
      this.showLogin = false;
    }
    this.getCredentials();
  }
  getCredentials(){
    if(this.employee.secondaryaddress!=null){
      this.showSecondaryAddress = true;
    }
  }
  goBack(): void {
    this.location.back();
  }
  goToLogin():void{
    this.localstorageService.removeData("profile");
    this.localstorageService.removeData("registerFormData");
    this.router.navigate(["/login"]);
  }
  printPage():void{
    window.print();
  }
  editProfile():void{
    if(this.employee!=null&&this.employee!=undefined){
      this.createRegisterFormData();
    }
    this.router.navigate(['/editprofile']);
  }
  createRegisterFormData():void{
    var registerFormData:RegisterForm={};
    registerFormData.firstname = this.employee.firstname;
    registerFormData.lastname = this.employee.lastname;
    registerFormData.addressline1 = this.employee.primaryaddress.addressLine;
    registerFormData.businessunit = this.employee.businessunit;
    registerFormData.city1 = this.employee.primaryaddress.city;
    registerFormData.country1 = this.employee.primaryaddress.country;
    registerFormData.email = this.employee.email;
    registerFormData.jobtitle = this.employee.jobtitle;
    registerFormData.state1 = this.employee.primaryaddress.state;
    registerFormData.telephone = this.employee.telephone;
    registerFormData.zipcode1 = this.employee.primaryaddress.zipcode;
    if(this.employee.secondaryaddress!=null&&this.employee.secondaryaddress!=undefined){
      registerFormData.addressline2 = this.employee.secondaryaddress.addressLine;
      registerFormData.city2 = this.employee.secondaryaddress.city;
      registerFormData.country2 = this.employee.secondaryaddress.country;
      registerFormData.state2 = this.employee.secondaryaddress.state;
      registerFormData.zipcode2 = this.employee.secondaryaddress.zipcode;
    }
    this.localstorageService.setData(registerFormData,"registerFormData");
  }
}
