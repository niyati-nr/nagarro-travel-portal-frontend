import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RequestStatus } from '../models/ticketrequest';
import { TicketRequestForm } from '../models/ticketrequestformmodel';
import { LocalstorageService } from '../services/localstorage.service';
import { TicketrequestService } from '../services/ticketrequest.service';

@Component({
  selector: 'app-adminaction',
  templateUrl: './adminaction.component.html',
  styleUrls: ['./adminaction.component.css']
})
export class AdminactionComponent implements OnInit {
  id:string;
  ticket: TicketRequestForm={};
  requestStatus=RequestStatus;
  keys = Object.keys;
  constructor(private localStorageService:LocalstorageService, private ticketRequestService:TicketrequestService, private router:Router, private route:ActivatedRoute) { }

  ngOnInit(): void {
    if(this.localStorageService.getData("employee")==null&&this.localStorageService.getData("employee")==undefined){
      this.router.navigate(['/login']);
    }
    if(this.localStorageService.getData("isAdmin")!="true"){
      this.router.navigate(['/dashboard']);
    }
    this.id = this.route.snapshot.paramMap.get('id');
  }
  async onSubmit(ngForm:NgForm){
    if(ngForm.value.request_status!=undefined){
      await this.ticketRequestService.changeStatus(this.id,ngForm.value.request_status)
      .subscribe(ticket=>{
        console.log(ticket);
      });
    }
    if(ngForm.value.admin_remarks!=undefined){
      await this.ticketRequestService.addRemarks(parseInt(this.id),ngForm.value.admin_remarks)
      .subscribe(ticket=>{
        console.log(ticket);
      });
    }
    alert("Ticket updated successfully.");
    this.router.navigate([`/ticketdetail/${this.id}`]);
  }
}
