import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { CovidResponse } from '../models/covidresponsemodel';

@Injectable({
  providedIn: 'root'
})
export class CovidtrackerService {
  constructor(private http: HttpClient) { }
  getStats(countryCode):Observable<CovidResponse>{
    const url = `https://disease.sh/v3/covid-19/countries/${countryCode}`;
    return this.http.get(url);
  }
}
