import { TestBed } from '@angular/core/testing';

import { TicketrequestService } from './ticketrequest.service';

describe('TicketrequestService', () => {
  let service: TicketrequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TicketrequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
