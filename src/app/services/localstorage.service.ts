import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  constructor() { }

setData(data:Object, key:string) {
    const jsonData = JSON.stringify(data);
    localStorage.setItem(key, jsonData);
 }
 
 getData(key:string) {
    return localStorage.getItem(key);
 }
 
 removeData(key) {
    localStorage.removeItem(key);
 }
}
