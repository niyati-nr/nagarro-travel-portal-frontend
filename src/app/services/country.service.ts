import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {Country} from '../models/countrymodel';
@Injectable({
  providedIn: 'root'
})
export class CountryService {
  country:Country={}
  constructor(private http: HttpClient) { }
  private countriesUrl = '/explorers-hub/';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  getCountries():Observable<Country[]>{
    const url=this.countriesUrl+"countries";
    return this.http.get<Country[]>(url).pipe(
      catchError(this.handleError<Country[]>('getCountries', []))
      );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
