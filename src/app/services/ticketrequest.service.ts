import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { TicketRequestForm } from '../models/ticketrequestformmodel';
import { TicketRequest } from '../models/ticketrequest';
@Injectable({
  providedIn: 'root'
})
export class TicketrequestService {

  constructor(private http:HttpClient) { }
  private ticketsUrl = '/explorers-hub/';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  getAllTickets():Observable<TicketRequest[]>{
    const url=this.ticketsUrl+"tickets";
    return this.http.get<TicketRequest[]>(url).pipe(
      catchError(this.handleError<TicketRequest[]>('getAllTickets', []))
      );
  }
  getActiveTickets():Observable<TicketRequest[]>{
    const url=this.ticketsUrl+"tickets/active";
    return this.http.get<TicketRequest[]>(url).pipe(
      catchError(this.handleError<TicketRequest[]>('getAllTickets', []))
      );
  }
  getTicketById(id:number):Observable<TicketRequest>{
    const url=this.ticketsUrl+"tickets/"+id;
    return this.http.get<TicketRequest>(url).pipe(
      catchError(this.handleError<TicketRequest>(`getTicketById`))
    );
  }
  getTicketsByEmployeeId(id:number):Observable<TicketRequest[]>{
    const url=this.ticketsUrl+"tickets/employee/"+id;
    return this.http.get<TicketRequest[]>(url).pipe(
      catchError(this.handleError<TicketRequest[]>('getTicketsByEmployeeId', []))
      );
  }
  raiseTicket(ticketRequestForm:TicketRequestForm, id:number):Observable<any>{
    const url = `${this.ticketsUrl}tickets/raiseticket/${id}`;
    return this.http.post(url,ticketRequestForm,this.httpOptions).pipe(
      catchError(this.handleError<any>('raiseTicket'))
    );;
  }
  editTicket(ticketRequestForm:TicketRequestForm, id:number):Observable<any>{
    const url = `${this.ticketsUrl}tickets/editticket/${id}`;
    return this.http.patch(url,ticketRequestForm,this.httpOptions).pipe(
      catchError(this.handleError<any>('editTicket'))
    );;
  }
  changeStatus(id,status){
    const url = `${this.ticketsUrl}tickets/changestatus/${id}?status=${status}`;
    return this.http.patch(url,this.httpOptions).pipe(
      catchError(this.handleError<any>('changeStatus'))
    );
  }
  addRemarks(id, remarks){
    const url = `${this.ticketsUrl}tickets/adminremarks/${id}?remarks=${remarks}`;
    return this.http.patch(url,this.httpOptions).pipe(
      catchError(this.handleError<any>('addRemarks'))
    );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
