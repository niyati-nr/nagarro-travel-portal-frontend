import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {Region} from '../models/regionmodel';
@Injectable({
  providedIn: 'root'
})
export class RegionService {
  region:Region={}
  constructor(private http:HttpClient) { }
  private regionsUrl = '/explorers-hub/';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  getRegions(id):Observable<Region[]>{
    const url=this.regionsUrl+"regions/country/"+id;
    return this.http.get<Region[]>(url).pipe(
      catchError(this.handleError<Region[]>('getRegions', []))
      );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
