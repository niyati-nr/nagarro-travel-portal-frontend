import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {LoginModel} from '../models/loginmodel';
import { RegisterForm } from '../models/registerformmodel';
import {Employee} from '../models/employeemodel';
import { removeAllListeners } from 'node:process';
import { LocalstorageService } from './localstorage.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient, private localStorageService:LocalstorageService) { }
  private employeesUrl = '/explorers-hub/';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  login(loginModel:LoginModel):Observable<any>{
    const url = this.employeesUrl+"employees/login";
    return this.http.post(url, loginModel, this.httpOptions).pipe(
      catchError(this.handleError<any>('login'))
    );
  }
  register(registerForm:RegisterForm):Observable<any>{
    const url = this.employeesUrl+"employees/register";
    return this.http.post(url, registerForm, this.httpOptions).pipe(
      catchError(this.handleError<any>('register'))
    );
  }
  editProfile(registerForm:RegisterForm, id:number){
    const url = `${this.employeesUrl}employees/${id}`;
    return this.http.patch(url, registerForm, this.httpOptions).pipe(
      catchError(this.handleError<any>('editProfile'))
    );
  }
  checkEmail(email:string):Observable<Number>{
    email = email.replace('"','');
    email = email.replace('"','');
    const url = `${this.employeesUrl}/employees/checkemail/${email}`;
    console.log(url);
    return this.http.get<Number>(url).pipe(
      catchError(this.handleError<Number>(`checkEmail`))
    );
  }
  forgotCredentials(id:Number){
    const url = `${this.employeesUrl}/employees/forgotcredentials/${id}`;
    return this.http.get(url,{observe: 'response'});
  }
  logOut(){
    this.localStorageService.removeData("employee");
    this.localStorageService.removeData("profile");
    this.localStorageService.removeData("registerFormData");
    this.localStorageService.removeData("isAdmin");
  }
  
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
