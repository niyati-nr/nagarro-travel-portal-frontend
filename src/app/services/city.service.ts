import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {City} from '../models/citymodel';
@Injectable({
  providedIn: 'root'
})
export class CityService {

  city:City={}
  constructor(private http:HttpClient) { }
  private citiesUrl = '/explorers-hub/';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  getCities(id):Observable<City[]>{
    const url=this.citiesUrl+"cities/region/"+id;
    return this.http.get<City[]>(url).pipe(
      catchError(this.handleError<City[]>('getCities', []))
      );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
