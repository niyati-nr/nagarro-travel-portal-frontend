import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EmployeedashboardComponent } from './employeedashboard/employeedashboard.component';
import { ForgotcredentialsComponent } from './forgotcredentials/forgotcredentials.component';
import { EmployeeprofileComponent } from './employeeprofile/employeeprofile.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { RaiseticketComponent } from './raiseticket/raiseticket.component';
import { TicketRequestsComponent } from './ticket-requests/ticket-requests.component';
import { TicketdetailComponent } from './ticketdetail/ticketdetail.component';
import { EditticketComponent } from './editticket/editticket.component';
import { AllticketsComponent } from './alltickets/alltickets.component';
import { ActiveticketsComponent } from './activetickets/activetickets.component';
import { AdminactionComponent } from './adminaction/adminaction.component';
import { HomePageComponent } from './home-page/home-page.component';
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {path: 'dashboard', component: EmployeedashboardComponent},
  {path: 'forgotcredentials', component: ForgotcredentialsComponent},
  {path: 'profile', component: EmployeeprofileComponent},
  {path: 'editprofile', component: EditprofileComponent},
  {path: 'raiseticket', component: RaiseticketComponent},
  {path: 'listtickets', component: TicketRequestsComponent},
  {path: 'ticketdetail/:id', component: TicketdetailComponent},
  {path: 'editticket/:id', component: EditticketComponent},
  {path: 'alltickets', component: AllticketsComponent},
  {path: 'activetickets', component: ActiveticketsComponent},
  {path: 'adminaction/:id', component: AdminactionComponent},
  {path: '', component: HomePageComponent},
  {path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
