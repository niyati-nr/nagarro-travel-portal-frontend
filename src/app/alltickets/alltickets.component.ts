import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Employee } from '../models/employeemodel';
import{RequestStatus, TicketRequest} from '../models/ticketrequest';
import { TicketRequestForm } from '../models/ticketrequestformmodel';
import { LocalstorageService } from '../services/localstorage.service';
import { TicketrequestService } from '../services/ticketrequest.service';
declare var $:any;
@Component({
  selector: 'app-alltickets',
  templateUrl: './alltickets.component.html',
  styleUrls: ['./alltickets.component.css']
})
export class AllticketsComponent implements OnInit {
  tickets:TicketRequest[]
  requestStatus = RequestStatus;
  ticket:TicketRequestForm={};
  p: number = 1;
  keys = Object.keys;
  constructor(private localStorageService:LocalstorageService ,private ticketRequestService:TicketrequestService, private router:Router) { }

  ngOnInit(): void {
    if(this.localStorageService.getData("employee")==null&&this.localStorageService.getData("employee")==undefined){
      this.router.navigate(['/login']);
    }
    if(this.localStorageService.getData("isAdmin")!="true"){
      this.router.navigate(['/dashboard']);
    }
    this.getAllickets();
  }
  async getAllickets(){
    await this.ticketRequestService.getAllTickets()
    .subscribe(tickets=>{
      this.tickets = tickets;
      this.tickets = this.tickets.reverse();
    })
  }
}
