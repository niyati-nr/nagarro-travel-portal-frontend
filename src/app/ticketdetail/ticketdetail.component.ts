import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { TicketRequest } from '../models/ticketrequest';
import { LocalstorageService } from '../services/localstorage.service';
import { TicketrequestService } from '../services/ticketrequest.service';

@Component({
  selector: 'app-ticketdetail',
  templateUrl: './ticketdetail.component.html',
  styleUrls: ['./ticketdetail.component.css']
})
export class TicketdetailComponent implements OnInit {
  id:string;
  ticketRequest:TicketRequest;
  constructor(private route:ActivatedRoute, private ticketRequestService:TicketrequestService, private localStorageService:LocalstorageService, private router:Router) { }

  ngOnInit(): void {
    if(JSON.parse(this.localStorageService.getData("employee"))==undefined&&JSON.parse(this.localStorageService.getData("emaployee"))==null){
      this.router.navigate(['/login']);
    }
    this.id = this.route.snapshot.paramMap.get('id');
    this.getTicket();
  }
   async getTicket(){
    await this.ticketRequestService.getTicketById(parseInt(this.id))
    .subscribe(ticketRequest=>{
      this.ticketRequest = ticketRequest;
    })
  }
  printPage():void{
    window.print();
  }
}
