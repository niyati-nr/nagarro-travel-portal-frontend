import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../models/employeemodel';
import{TicketRequest, RequestType, RequestPriority, RequestStatus,ExpenseHandler} from '../models/ticketrequest';
import { LocalstorageService } from '../services/localstorage.service';
import { TicketrequestService } from '../services/ticketrequest.service';
@Component({
  selector: 'app-ticket-requests',
  templateUrl: './ticket-requests.component.html',
  styleUrls: ['./ticket-requests.component.css']
})
export class TicketRequestsComponent implements OnInit {
  employee:Employee;
  tickets:TicketRequest[]
  p: number = 1;
  constructor(private localStorageService:LocalstorageService,private ticketRequestService:TicketrequestService, private router:Router) { }

  ngOnInit(): void {
    if(JSON.parse(this.localStorageService.getData("employee"))==undefined&&JSON.parse(this.localStorageService.getData("emaployee"))==null){
      this.router.navigate(['/login']);
    }
    if(this.localStorageService.getData("isAdmin")=="true"){
      this.router.navigate(['/alltickets']);
    }
    this.employee = JSON.parse(this.localStorageService.getData("employee"));
    this.getEmployeeTickets();
  }
  async getEmployeeTickets(){
    await this.ticketRequestService.getTicketsByEmployeeId(this.employee.emp_id)
    .subscribe(tickets=>{
      this.tickets = tickets;
      this.tickets = this.tickets.reverse();
    })
  }

}
