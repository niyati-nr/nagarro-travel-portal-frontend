import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CountryService } from '../services/country.service';
import { RegionService } from '../services/region.service';
import { CityService } from '../services/city.service';
import { Country } from '../models/countrymodel';
import { Region } from '../models/regionmodel';
import { City } from '../models/citymodel';
import {RequestStatus, RequestPriority, RequestType, TicketRequest, ExpenseHandler} from '../models/ticketrequest';
import { EmployeeService } from '../services/employee.service';
import { LocalstorageService } from '../services/localstorage.service';
import { DatePipe } from '@angular/common';
import {TicketRequestForm} from '../models/ticketrequestformmodel';
import { TicketrequestService } from '../services/ticketrequest.service';
import { Employee } from '../models/employeemodel';
import { CovidResponse } from '../models/covidresponsemodel';
import { CovidtrackerService } from '../services/covidtracker.service';
import {
  ApexAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexDataLabels,
  ApexPlotOptions,
  ApexYAxis,
  ApexTitleSubtitle,
  ApexXAxis,
  ApexFill
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  fill: ApexFill;
  title: ApexTitleSubtitle;
};
@Component({
  selector: 'app-raiseticket',
  templateUrl: './raiseticket.component.html',
  styleUrls: ['./raiseticket.component.css']
})
export class RaiseticketComponent implements OnInit {
  ticketRequestForm:TicketRequestForm={}
  requestStatus = RequestStatus;
  requestPriority = RequestPriority;
  requestType = RequestType;
  expenseHandler = ExpenseHandler;
  countries:Country[]=[]
  regions1:Region[]=[]
  cities1:City[]=[]
  regions2:Region[]=[]
  cities2:City[]=[]
  showRegions1=false;
  showCities1=false;
  showRegions2=false;
  showCities2=false;
  selectedCountry:Country;
  employee:Employee;
  ticketRequest:TicketRequest;
  covidResponse:CovidResponse;
  showCovidResponse:boolean=false;
  keys = Object.keys;
  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  constructor(private countryService:CountryService, private regionService:RegionService, private cityService:CityService, private route: ActivatedRoute,private router: Router, private datePipe:DatePipe, private ticketRequestService:TicketrequestService, private localStorageService:LocalstorageService, private covidtrackerService:CovidtrackerService) {
    
   }
  ngOnInit(): void {
    if(JSON.parse(this.localStorageService.getData("employee"))==undefined&&JSON.parse(this.localStorageService.getData("emaployee"))==null){
      this.router.navigate(['/login']);
    }
    if(this.localStorageService.getData("isAdmin")=="true"){
      this.router.navigate(['/alltickets']);
    }
    this.employee = JSON.parse(this.localStorageService.getData("employee"));
    this.getCountries();
  }

  onSubmit(ngForm:NgForm){
    this.ticketRequestForm.request_priority = ngForm.value.request_priority;
    this.ticketRequestForm.request_type = ngForm.value.request_type;
    this.ticketRequestForm.additional_details = ngForm.value.additional_details;
    this.ticketRequestForm.amount_limit = ngForm.value.amount_limit;
    this.ticketRequestForm.destination_city = ngForm.value.city2.id;
    this.ticketRequestForm.origin_city = ngForm.value.city1.id;
    this.ticketRequestForm.end_date = this.datePipe.transform(new Date(ngForm.value.end_date),"dd/MM/yyyy");
    this.ticketRequestForm.start_date = this.datePipe.transform(new Date(ngForm.value.start_date),"dd/MM/yyyy");
    this.ticketRequestForm.expense_handler = ngForm.value.expense_handler;
    this.ticketRequestForm.passport_number = ngForm.value.passport_number;
    this.ticketRequestForm.project_name = ngForm.value.project_name;
    this.ticketRequestForm.travel_approver = ngForm.value.travel_approver;
    this.ticketRequestForm.travel_duration = ngForm.value.travel_duration;
    this.raiseTicket();
  }
  onChangeCity(registerForm:NgForm){
    // console.log(registerForm.value);
    //do nothing
  }
  async onChangeRegion(newRegionId,elementName:string){
    // if(elementName=="region1"){
    //   this.PrimaryAddressSpinner.nativeElement.style.display="block";
    // }
    // else{
    //   this.SecondaryAddressSpinner.nativeElement.style.display="block";
    // }
    console.log(newRegionId);
    await this.cityService.getCities(newRegionId)
    .subscribe(cities=>{
      if(elementName=="region1"){
        this.cities1=cities;
        this.showCities1=true;
        // this.PrimaryAddressSpinner.nativeElement.style.display="none";
      }
      else if(elementName=="region2"){
        this.cities2=cities;
        this.showCities2=true;
        // this.SecondaryAddressSpinner.nativeElement.style.display="none";
      }
    });
  }
  async onChangeCountry(selectValue,elementName:string){
    var newCountryId:number;
    if(elementName=="country1"){
      // this.PrimaryAddressSpinner.nativeElement.style.display="block";
      newCountryId = selectValue;
    }
    else{
      // this.SecondaryAddressSpinner.nativeElement.style.display="block";
      this.showCovidResponse=false;
      newCountryId = selectValue.id;
      await this.getCovidStats(selectValue.code);
      setTimeout(() => {
        console.log(selectValue);
        console.log(this.covidResponse);
        if(this.covidResponse!=null&&this.covidResponse!=undefined){
          this.chartOptions = {
            series: [
              {
                name: "Covid Statistics",
                data: [this.covidResponse.cases,this.covidResponse.active, this.covidResponse.recovered, this.covidResponse.deaths]
              }
            ],
            chart: {
              height: 350,
              type: "bar"
            },
            plotOptions: {
              bar: {
                dataLabels: {
                  position: "top" // top, center, bottom
                }
              }
            },
            dataLabels: {
              enabled: true,
              formatter: function(val) {
                return val;
              },
              offsetY: -20,
              style: {
                fontSize: "12px",
                colors: ["#304758"]
              }
            },
      
            xaxis: {
              categories: [
                "Total Cases",
                "Active Cases",
                "Recovered",
                "Deaths"
              ],
              position: "top",
              labels: {
                offsetY: -18
              },
              axisBorder: {
                show: false
              },
              axisTicks: {
                show: false
              },
              crosshairs: {
                fill: {
                  type: "gradient",
                  gradient: {
                    colorFrom: "#D8E3F0",
                    colorTo: "#BED1E6",
                    stops: [0, 100],
                    opacityFrom: 0.4,
                    opacityTo: 0.5
                  }
                }
              },
              tooltip: {
                enabled: true,
                offsetY: -35
              }
            },
            fill: {
              type: "gradient",
              gradient: {
                shade: "light",
                type: "horizontal",
                shadeIntensity: 0.25,
                gradientToColors: undefined,
                inverseColors: true,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [50, 0, 100, 100]
              }
            },
            yaxis: {
              axisBorder: {
                show: false
              },
              axisTicks: {
                show: false
              },
              labels: {
                show: false
              }
            },
            title: {
              text: "Covid-19 statistics: "+selectValue.name,
              floating: false,
              offsetY: 320,
              align: "center",
              style: {
                color: "#444"
              }
            }
          };
          this.showCovidResponse = true;
        }
      }, 1500);     
    }
    await this.regionService.getRegions(newCountryId)
    .subscribe(regions=>{
      if(elementName=="country1"){
        this.regions1=regions;
        this.showRegions1=true;
        // this.PrimaryAddressSpinner.nativeElement.style.display="none";
      }
      else if(elementName=="country2"){
        this.regions2=regions;
        this.showRegions2=true;
        // this.SecondaryAddressSpinner.nativeElement.style.display="none";
      }
    });
  }
  async getCountries(){
    await this.countryService.getCountries()
    .subscribe(countries=>this.countries=countries);
  }
  async raiseTicket(){
    await this.ticketRequestService.raiseTicket(this.ticketRequestForm,this.employee.emp_id).subscribe(ticketRequest => {
      this.ticketRequest = ticketRequest;
      if(this.ticketRequest==null||this.ticketRequest==undefined){
        alert("Error Raising Ticket. Please try again.");
        window.location.reload();
      }
      else{
        alert("Ticket raised successfully.\n\nThe details have been mailed to your e-mail address.");
        this.router.navigate(['/listtickets']);
      }
    });
  }
  async getCovidStats(code){
    await this.covidtrackerService.getStats(code)
    .subscribe(covidResponse=>{
      this.covidResponse = covidResponse;
    })
  }
}
